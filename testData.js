var defaultJson = {
    a:1,
    "b":{
        c:{
            "d":[1,2,3]
        },
        f:[
            {g4:1},
            {g5:1},
            {g6:1},
            {   
                g:2,
                g1:[{h:3},{h1:4},{h2:5}],
                g2:21,
                g3:22
            }
        ]
    },
    z:100
};

var testData = [{
        oriJson: defaultJson,
        tarJson: {
            a:1,
            "b":{
                c:{
                    "d":[1,2,3]
                },
                f:[
                    {g4:1},
                    {g5:1},
                    {g6:1},
                    {
                        g1:[{h:3},{h1:4}],
                        g2:21,
                        g3:22
                    }
                ]
            },
            z:100
        },
        expect: ['b.f.array.g', 'b.f.array.g1.array.h2']        
    },
    {
        oriJson: defaultJson,
        tarJson: {
            a:1,
            "b":{
                f:[
                    {g6:1},
                    {
                        g1:[{h:3},{h1:4}],
                        g3:22
                    }
                ]
            },
            z:100
        },
        expect:  ['b.c',           
        'b.f.array.g4',
        'b.f.array.g5',
        'b.f.array.g',
        'b.f.array.g2',
        'b.f.array.g1.array.h2']
              
    },
    {
        oriJson: defaultJson,
        tarJson: {
            a:1,
            "b":"",
            z:100
        },
        expect: ['b.c', 'b.f']        
    },
    {
        oriJson:{
            a:'a',
            b:{
                b1:'b1',
                b2:'b2'
            }
        },
        tarJson:{
            a:'a',
            b:{
                b1:'b1'
            }
        },
        expect: ['b.b2']        
    }
];

module.exports = testData;