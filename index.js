
/**
* @param json: curent json object
* @param map: result data map, e.g. {level:Array} => {1:['a.b', 'a.c']}
* @param pkey: parent key, e.g. a.b.xxx
* @param l: current level
* @param index: array index
*/
function deepTraverse(json, map, pkey, l, index){
    //object
    if(isType(json, "Object")){
        ++l;

        //if obj of array,do not do it
        if(!isNaN(index) && index >= 0){
            //check is first time
            if(!isType(map[l], "Array")){
                map[l] = [];  
            }
        }else{
            map[l] = []; 
        }
        
        for(var key in json){
            var curKey = key;
            //if array, append array[index]
            if(!isNaN(index) && index >= 0){
                //curKey = 'array[' + index + '].' + key;
                curKey = 'array.' + key;
            }
            //append a.b.c.xxx for k
            var k = pkey ? pkey + '.' + curKey : curKey;
            if(!map[l].includes(k)){
                map[l].push(k);
            }
            //console.log('cur json: ', json, l, k);
            //check value
            if(isType(json[key], "Array") || isType(json[key], "Object")){
                deepTraverse(json[key], map, k, l);
            }
        }
    }
    // array
    else if(isType(json, "Array")){
        json.forEach(function(jsonObj, index){
            deepTraverse(jsonObj, map, pkey, l, index);
        });
    }
}

/**
* type validation
* @param obj object
* @param type expect type
*/
function isType(obj,type){
    return Object.prototype.toString.call(obj) === "[object "+type+"]";
}

/**
* @param oriMap origin(mock) data map 
* @param tarMap target(real) data map
* @return different keys of json
*/
function compareJsonKeys(oriMap,tarMap){
    var diffArr = [];
    for(var o in oriMap){
        var oriArr = oriMap[o],
            tarArr = tarMap[o];

        for(var item of oriArr){

            if(!tarArr){
                !isContain(diffArr, item) && diffArr.push(item);
                continue;
            }
                
            var isHas = tarArr.includes(item);
            if(!isHas){
                !isContain(diffArr, item) && diffArr.push(item);
                //console.log(o, item, isContain);
            }
        }
    }

    return diffArr;
}


/**
 * check arr is contain the item 
 * @param {*} arr  
 * @param {*} item 
 * @return true if contain, false dont contain
 */
function isContain(arr, item){
    var isContain = false;
    for(var diffItem of arr){
        //for fullmatch need to add . as end code
        if(item.indexOf(diffItem + '.') === 0){
            isContain = true;
            return isContain;
        }
    }
}

module.exports = {
    convertJsonToMap:deepTraverse,
    compareJsonKeys:compareJsonKeys
}