var testData = require ('./testData.js'),
    util = require ('./index.js');

//test data
var data = testData[3];

//origin 
var oriMap = {};
util.convertJsonToMap(data.oriJson, oriMap, '', 0);
//console.log('oriMap:', oriMap);

//target 
var tarMap = {};
util.convertJsonToMap(data.tarJson, tarMap, '', 0);
//console.log('tarMap:',tarMap);

var result = util.compareJsonKeys(oriMap, tarMap);
console.log('compareJsonKeys:',result);
if(data.expect.toString() == result.toString()){
    console.log('=============Passed==============');
}else{
    console.log('=============Failed==============');
}
