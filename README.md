Deep compare all keys of two jsons without dependent other library
    
main.js & testData.js are for testing
    
execute `node main.js`


    var oriJson = {
        a:'a',
        b:{
            b1:'b1',
            b2:'b2'
        }
    } 
    

    var tarJson = {
        a:'a',
        b:{
            b1:'b1'
        }
    }



`b.b2` is different key
